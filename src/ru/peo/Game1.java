package ru.peo;

/**
 * Created by Elena on 26.9.2017.
 */
public class Game1 {
    Player p1;
    Player p2;
    Player p3;

    public void startGame() {
        p1 = new Player();
        p2 = new Player();
        p3 = new Player();
        int guessp1 = 0;
        int guessp2 = 0;
        int guessp3 = 0;
        boolean p1isRight = false;
        boolean p2isRight = false;
        boolean p3isRight = false;
        int targetNumber = (int) (Math.random() * 10);
        System.out.println("Я думаю это число от 0 до 9...");
        while(true) {
            System.out.println("Загаданное число " + targetNumber);

            p1.guess();
            p2.guess();
            p3.guess();

            guessp1 = p1.number;
            System.out.println("Первый игрок загадал " + guessp1);
            guessp2 = p2.number;
            System.out.println("Второй игрок загадал " + guessp2);
            guessp3 = p3.number;
            System.out.println("Третий игрок загадал " + guessp3);

            if (guessp1 == targetNumber) {
                p1isRight = true;
            }
            if (guessp2 == targetNumber) {
                p2isRight = true;
            }
            if (guessp3 == targetNumber) {
                p3isRight = true;
            }

            if (p1isRight || p2isRight || p3isRight)
            {
                System.out.println("У нас есть победитель!");
                System.out.println("1 игрок победитель? " + p1isRight);
                System.out.println("2 игрок победитель? " + p2isRight);
                System.out.println("3 игрок победитель? " + p3isRight);
                System.out.println("Игра окончена");
                break;
            }
            else
            {
                System.out.println("Игрокам придется повторить попытку.");
            }
        }
    }
}